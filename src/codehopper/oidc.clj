;;   Copyright (c) Carlo Sciolla. All rights reserved.
;;   The use and distribution terms for this software are covered by the
;;   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;;   which can be found in the file epl-v10.html at the root of this distribution.
;;   By using this software in any fashion, you are agreeing to be bound by
;;   the terms of this license.
;;   You must not remove this notice, or any other, from this software.

(ns codehopper.oidc
  "OpenID connect based authentication. Currently only supporting the Authorization Code flow, with both the access
  and ID tokens stored as cookies. Refreshing the access token is not currently supported"
  (:require [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.java.io :as io]

            [buddy.core.codecs.base64 :as b64]
            [buddy.core.codecs :as codecs]
            [buddy.sign.jws :as jws]

            [clj-http.client :as client]
            [clj-http.conn-mgr :as conn]

            [ring.util.response :as response]
            [ring.util.request :as request]
            [ring.util.codec :as codec])
  (:import (java.security KeyFactory Key Signature SecureRandom)
           (java.security.spec RSAPublicKeySpec)))

;;
;; HTTP utilities
;;

(def ^:private connection-manager
  (conn/make-reusable-conn-manager {}))

(def ^:private caching-http-client
  (atom nil))

(defn- cached-http-get [url]
  (if-let [client @caching-http-client]
    (:body (client/get url {:connection-manager connection-manager
                            :http-client        client
                            :cache              true}))
    (let [{:keys [http-client body]} (client/get url {:connection-manager connection-manager
                                                      :cache              true})]
      (reset! caching-http-client http-client)
      body)))

;;
;; JSON utilities
;;

(defn parse-json
  "Parses a string representing jsons, using kebab-case keywords in place of snake-case strings, e.g.:
  (parse-json \"{\"my_key\": 42}\") => {:my-key 42}"
  [json-str]
  (letfn [(snake->kebab [s] (string/replace s #"_" "-"))]
    (json/read-str json-str :key-fn (comp keyword snake->kebab))))

(defn slurp-json
  "Parses the json obtained by slurping the provided URL"
  [url]
  (parse-json (cached-http-get url)))

;;
;; Known OpenID discovery documents, see https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderConfig
;;

(def ^:const google-discovery-url
  "https://accounts.google.com/.well-known/openid-configuration")

(def ^:const microsoft-discovery-url
  "https://login.microsoftonline.com/fabrikamb2c.onmicrosoft.com/v2.0/.well-known/openid-configuration")

(def ^:const yahoo-discovery-url
  "https://login.yahoo.com/.well-known/openid-configuration")

(def ^:const paypal-discovery-url
  "https://www.paypalobjects.com/.well-known/openid-configuration")

(def ^:const salesforce-discovery-url
  "https://login.salesforce.com/.well-known/openid-configuration")

(def ^:dynamic *discovery-url*)

;;
;; OpenID parameters configuration
;;

(defn ^:private from-discovery-document
  "Retrieves a given configuration option at the given key from the Google OpenID discovery document.
  The document should provide the following keys:
  - :revocation-endpoint
  - :subject-types-supported
  - :token-endpoint
  - :userinfo-endpoint
  - :jwks-uri
  - :code-challenge-methods-supported
  - :response-types-supported
  - :authorization-endpoint
  - :token-endpoint-auth-methods-supported
  - :id-token-signing-alg-values-supported
  - :issuer
  - :scopes-supported
  - :claims-supported"
  [discovery-url key]
  (-> (slurp-json discovery-url)
      (get key)))

(defn oauth-base-url
  "Retrieves the :authorization-endpoint from the Goople OpenID discovery document"
  [discovery-url]
  (from-discovery-document discovery-url :authorization-endpoint))

(defn oauth-token-url
  "Retrieves the :token-endpoint from the Goople OpenID discovery document"
  [discovery-url]
  (from-discovery-document discovery-url :token-endpoint))

(defn jwks-url
  "Retrieves the :jwks-uri from the Goople OpenID discovery document"
  [discovery-url]
  (from-discovery-document discovery-url :jwks-uri))

;;
;; Encryption utilities
;;

(extend-protocol codecs/IByteArray
  Key
  (-to-bytes [this] (.getEncoded this)))

(defn get-public-key
  "Creates the public key out of the provided JWKS key specification. Only supports RSA"
  [{:keys [e n]}]
  (let [modulus  (BigInteger. 1 (b64/decode n))
        exponent (BigInteger. 1 (b64/decode e))
        kf       (KeyFactory/getInstance "RSA")]
    (.generatePublic kf (RSAPublicKeySpec. modulus exponent))))

(defn resolve-key
  "Takes all the keys from the published JWKS manifest and retrieves the first one that matches the
  alg / kid / typ specification from the JWT header. Returns nil otherwise.
  Suitable to be used as key provider for buddy.sign.util/resolve-key"
  [{:keys [alg kid typ]}]
  (let [jwks      (:keys (slurp-json (jwks-url *discovery-url*)))
        [key & _] (filter (comp #{kid} :kid) jwks)]
    (get-public-key key)))

(defn state-token
  "Generates a random token used to validate the completion of an OAuth flow as a genuine user attempt to access
  the application"
  []
  (->> (SecureRandom.)
       (BigInteger. 130)
       (str)))

(defn parse-id-token
  "Validates the signature of an ID token and extracts the contents of the ID token claims"
  [id-token]
  (when id-token
    (try
      (let [{:keys [alg]} (jws/decode-header id-token)
            dough         (jws/unsign id-token resolve-key {:alg alg})]
        (json/read (io/reader dough)))
      (catch Exception ex
        (throw (ex-info "Could not validate the token" {:id-token id-token} ex))))))

;;
;; OAuth handshake
;;

(defn compose-oauth-url
  "Generates the full URL to redirect the user agent to in order to initiate the Google login"
  [client-id state redirect scope]
  (let [base (oauth-base-url *discovery-url*)]
    (str base
         "?client_id="    client-id
         "&state="        (codec/url-encode state)
         "&redirect_uri=" redirect
         "&response_type=code"
         "&scope="        scope)))

(defn compose-token-url
  "Generates the full URL to redirect the user agent to in order to obtain the authentication token"
  [client-id client-secret redirect code]
  (let [base (oauth-token-url *discovery-url*)]
    (str base
         "?client_id="      client-id
         "&client_secret="  client-secret
         "&code="           code
         "&redirect_uri="   redirect
         "&authorization_code")))

(defn compose-callback-url
  "Generates the callback URL where Google will redirect the user agent after login as follows:
  `http[s]://<current-server>/cb`. This URL must be setup as a valid callback URL in the Google OAuth
  configuration"
  [{:keys [server-name] :as req}]
  (let [scheme (or (get-in req [:headers "x-forwarded-proto"])
                   (name (:scheme req)))]
    (str scheme "://" server-name "/cb")))

(defn- access-tokens
  "Extracts the tokens from the JSON returned by the Google token endpoint"
  [body]
  (-> body
      parse-json
      (select-keys [:access-token :id-token :expires-in])))

(defn- request-access-tokens
  "Exchanges an authorization code for an access token and an ID token"
  [client-id client-secret redirect code]
  (let [form-params {:code          code
                     :client_id     client-id
                     :client_secret client-secret
                     :redirect_uri  redirect
                     :grant_type    "authorization_code"}
        token-resp  (client/post (oauth-token-url *discovery-url*) {:form-params form-params})]
    (when (= 200 (:status token-resp))
      (access-tokens (:body token-resp)))))

(defn store-state-in-session
  "Stores the local state of an OAuth request into the session"
  [state
   {request-session :session :as request}
   {response-session :session :as response}]
  (if (some? response-session)
    (assoc-in response [:session ::state] state)
    (->> (assoc request-session ::state state)
         (assoc response :session))))

(defn get-state-from-session
  "Retrieves the saved state in the session"
  [req _]
  (get-in req [:session ::state]))

(defn gen-state
  "Generates the OAuth `state` to store a randomly generated number as well as the URL of the current request"
  [{:keys [uri query-string]} _]
  (let [csrf-token  (state-token)
        current-url (string/join "?" (filter some? [uri query-string]))]
    (str csrf-token "|" current-url)))

(defn start-oauth-flow
  "Redirects to the Google login screen to start the OAuth flow"
  [req {:keys [callback-url store-state client-id state-fn scope-fn]
        :or   {store-state store-state-in-session
               state-fn    gen-state
               scope-fn    (constantly "openid email")}
        :as   opts}]
  (let [state (gen-state req nil)
        scope (scope-fn req opts)]
    (->> (response/redirect (compose-oauth-url client-id state callback-url scope))
         (store-state state req))))

(defn current-domain
  "Extracts the domain addressed by the given ring request"
  [{:keys [server-name] :as req}]
  server-name)

(defn saved-trigger-url
  "Retrieves the URL that triggered the start of the OAuth flow from the OAuth `state`"
  [req {:keys [retrieve-state]
        :or   {retrieve-state get-state-from-session}
        :as   opts}]
  (let [state           (retrieve-state req opts)
        [_ trigger-url] (string/split state #"\|")]
    trigger-url))

(defn valid-state?
  "Validates the state which is sent back to the app after a successful authentication"
  [req
   {:keys [retrieve-state]
    :or {retrieve-state get-state-from-session} :as opts}
   state]
  (= state (retrieve-state req opts)))

(defn set-default-cookies
  "Sets the default `access_token` and `id_token` cookies, handling expiration, domain, secure and same site
  options following the provided `opts`"
  [req {:keys [access-token id-token expires-in]} {:keys [redirect-to retrieve-state
                                                          cookie-max-age cookie-secure cookie-domain
                                                          cookie-path cookie-same-site cookie-set]
                                                   :or   {cookie-domain    (current-domain req)
                                                          redirect-to      (saved-trigger-url req opts)
                                                          cookie-max-age   expires-in
                                                          cookie-secure?   true
                                                          cookie-same-site :lax}
                                                   :as   opts}]
  (let [cookie-opts (cond-> {:http-only true
                             :max-age   cookie-max-age}
                      (some? cookie-domain)
                      (assoc :domain cookie-domain)

                      (true? cookie-secure)
                      (assoc :secure true)

                      (some? cookie-path)
                      (assoc :path cookie-path)

                      (some? cookie-same-site)
                      (assoc :same-site cookie-same-site))]
    (-> (response/redirect redirect-to)
        (response/set-cookie "access_token" access-token cookie-opts)
        (response/set-cookie "id_token" id-token cookie-opts))))

(defn- back-to-app
  "Passes control back to the app at the same URL that triggered the initial OAuth flow. Stores the access token
  and ID token in a cookie set for the provided domain, or the current domain if none is given."
  [req tokens {:keys [cookie-set]
               :or   {cookie-set set-default-cookies}
               :as   opts}]
  (cookie-set req tokens opts))

;;
;; OAuth context inspection
;;

(defn get-default-cookies
  "Retrieves the cookies set by the default OAuth implementation to hold the access and ID tokens"
  [req opts]
  (let [access-token (get-in req [:cookies "access_token"])
        id-token     (get-in req [:cookies "id_token"])]
    (cond-> nil
      (some? access-token)
      (assoc ::access-token (:value access-token)
             ::expires-in   (:max-age access-token))

      (some? id-token)
      (assoc ::id-token (parse-id-token (:value id-token))))))

(defn valid-tokens?
  "Validates the ID token, see https://developers.google.com/identity/protocols/OpenIDConnect#validatinganidtoken"
  [{:keys [id-token]} {:keys [client-id]}]
  (let [{:strs [iss aud exp]} (parse-id-token id-token) ; `parse-id-token` already verifies the signature
        now-in-seconds        (-> (java.util.Date.)
                                  (.getTime)
                                  (/ 1000))]
    (and (#{"https://accounts.google.com" "accounts.google.com"} iss)
         (= aud client-id)
         (> exp now-in-seconds))))

;;
;; Ring support
;;

(defn wrap-oidc
  "Ring middleware that validates the current OAuth credentials and either allows access or starts the OAuth
  workflow. Requires the `wrap-params` middleware, and in its default configuration also `wrap-session`. If  you
  supply your implementations for both `:store-state` and `:retrieve-state` that don't use the session, then the
  `wrap-session` middleware is not required.

  A number of options can be provided, of which two are the mandatory OAuth credentials. Whenever functions can
  be provided that accept an `opts` parameter, they will receive the whole options map provided to `wrap-oidc`

  ## `:client-id` (mandatory)
  The OAuth `client-id`

  ## `:client-secret` (mandatory)
  The OAuth `client-secret`

  ## `:discovery-url` (optional)
  The URL to the OIDC discovery document
  Default: `google-discovery-url`

  ## `:unauthorized-handler` (optional)
  A `(fn [request opts])` that is called when the current request is not authenticated
  Default: `start-oauth-flow`

  ## `:callback-url` (optional)
  The `redirect` value to pass to the OAuth provider.
  Default: the result of `compose-callback-url` applied to the current request

  ## `:retrieve-state` (optional)
  A `(fn [request opts])` used to retrieve the OAuth `state`
  Default: `get-state-from-session`

  ## `:store-state` (optional)
  A `(fn [request opts])` used to store the OAuth `state`
  Default: `store-state-in-session`

  ## `:state-fn` (optional)
  A `(fn [request opts])` used to generate the OAuth `state`

  ## `:redirect-to` (optional)
  The URL where to redirect the user after a successful login
  Default: the URL of the request where the OAuth workflow started

  ## `:scope-fn` (optional)
  A `(fn [request opts])` used to generate the OAuth scope to use when requesting an access token
  Default: `(constantly \"openid email\")`

  ## `:cookie-set` (optional)
  A `(fn [request tokens opts])` that is used to set the cookies holding the retrieved `tokens`, itself a map with
  `:access-token`, `:id-token` and `:expires-in` keys. All other `:cookie-*` options are only meaningful if the
  default implementation is used
  Default: `set-default-cookies`

  ## `:cookie-get` (optional)
  A `(fn [request opts])` that is used to retrieve the token cookies. Usually needed in combination with a custom
  `:cookie-set`. Must return a map of at least `::access-token`, `::id-token` and `::expires-in`
  Default: `get-default-cookies`

  ## `:cookie-domain` (optional)
  The domain on which to set the cookies holding the OAuth tokens. Only used with the default implementation for
  `:cookie-set`
  Default: the domain of the request where the OAuth workflow started

  ## `:cookie-max-age` (optional)
  The max age in seconds to set on the token cookies. Only used with the default implementation for `:cookie-set`
  Default: the `:expires-in` passed along the token response from the OAuth provider

  ## `:cookie-secure` (optional)
  Whether or not to enforce secure-only token cookies. Only used with the default implementation for `:cookie-set`
  Default: `true`

  ## `:cookie-path` (optional)
  Path to which token cookies need to be sent. Only used with the default implementation for
  `:cookie-set`
  Default: `nil`, meaning the token cookies are passed according to the `:domain`

  ## `:cookie-same-site` (optional)
  Pass in `:lax` if you want CORS requests pointing to the app to include the token cookies, or `:strict`
  to avoid that. Only used with the default implementation for `:cookie-set`
  Default: `:lax`"
  [handler
   {:keys [client-id client-secret callback-url unauthorized-handler cookie-get discovery-url]
    :or   {unauthorized-handler start-oauth-flow
           cookie-get           get-default-cookies
           discovery-url        google-discovery-url}
    :as   opts}]
  (fn [req]
    (binding [*discovery-url* discovery-url]
      (let [callback-url  (or callback-url
                              (compose-callback-url req))
            request-url   (request/request-url req)
            without-query (fn [url] (string/replace url #"\?.*$" ""))
            opts          (assoc opts :callback-url callback-url)
            code          (get-in req [:params "code"])
            state         (get-in req [:params "state"])
            cookie        (cookie-get req opts)]
        (cond
          ;; `code` is received by the callback after a successful Google login, we're almost authenticated
          (and (some? code)
               (= callback-url
                  (without-query request-url))
               (valid-state? req opts state))
          (let [access-tokens (request-access-tokens client-id client-secret callback-url code)]
            (if (valid-tokens? access-tokens opts)
              (back-to-app req access-tokens opts)
              (unauthorized-handler req opts)))

          ;; if we have valid tokens then we are already authenticated
          (some? cookie)
          (-> req
              (merge cookie)
              (handler))

          ;; in any other case authentication failed
          :else
          (unauthorized-handler req opts))))))
