# `oily`

An OpenID Connect implementation as a [`ring`][ring] middleware

<img src="https://gitlab.com/codehopper/oily/raw/master/doc/logo.png" width="120" height="120" />

> To keep a lamp burning, we have to keep putting oil in it<br/>
> -- Mother Theresa

[OpenID Connect][oidc] authentication for mere mortals.

## Rationale

Because it should already have existed?

## Usage

The library provides a highly customizable [`ring`][ring] middleware that you can use to add OIDC authentication
to your web app. The middleware requires both [`wrap-cookies`][cook] and, when using the default state management,
[`wrap-session`][sess]. You can pass a map of configuration option to the middleware like in the
[sample project][samp]:

```clojure
(-> my-handler
    (oidc/wrap-oidc {;; the mandatory OAuth credentials
                     :client-id     client-id
                     :client-secret secret

                     ;; make local testing easier by skipping SSL
                     :cookie-secure false

                     ;; override some defaults
                     :redirect-to   "/"
                     :callback-url  "http://local.codehopper.nl/cb"})
                    (wrap-params)
                    (wrap-cookies)
                    (wrap-session))
```

The full list of configurable options is provided in the following sections.

### `:client-id` (mandatory)
The OAuth `client-id`

### `:client-secret` (mandatory)
The OAuth `client-secret`

### `:discovery-url` (optional)
The URL to the OIDC discovery document

Default: `google-discovery-url`

### `:unauthorized-handler` (optional)
A `(fn [request opts])` that is called when the current request is not authenticated

Default: `start-oauth-flow`

### `:callback-url` (optional)
The `redirect` value to pass to the OAuth provider.

Default: the result of `compose-callback-url` applied to the current request

### `:retrieve-state` (optional)
A `(fn [request opts])` used to retrieve the OAuth `state`

Default: `get-state-from-session`

### `:store-state` (optional)
A `(fn [request opts])` used to store the OAuth `state`

Default: `store-state-in-session`

### `:state-fn` (optional)
A `(fn [request opts])` used to generate the OAuth `state`

Default: `gen-state`

### `:redirect-to` (optional)
The URL where to redirect the user after a successful login

Default: the URL of the request where the OAuth workflow started

### `:scope-fn` (optional)
A `(fn [request opts])` used to generate the OAuth scope to use when requesting an access token

Default: `(constantly \"openid email\")`

### `:cookie-set` (optional)
A `(fn [request tokens opts])` that is used to set the cookies holding the retrieved `tokens`, itself a map
with `:access-token`, `:id-token` and `:expires-in` keys. All other `:cookie-*` options are only meaningful i
f the default implementation is used

Default: `set-default-cookies`

### `:cookie-get` (optional)
A `(fn [request opts])` that is used to retrieve the token cookies. Usually needed in combination with a custom
`:cookie-set`. Must return a map of at least `::access-token`, `::id-token` and `::expires-in`

Default: `get-default-cookies`

### `:cookie-domain` (optional)
The domain on which to set the cookies holding the OAuth tokens. Only used with the default implementation for `:cookie-set`

Default: the domain of the request where the OAuth workflow started

### `:cookie-max-age` (optional)
The max age in seconds to set on the token cookies. Only used with the default implementation for `:cookie-set`

Default: the `:expires-in` passed along the token response from the OAuth provider

### `:cookie-secure` (optional)
Whether or not to enforce secure-only token cookies. Only used with the default implementation for `:cookie-set`

Default: `true`

### `:cookie-path` (optional)
Path to which token cookies need to be sent. Only used with the default implementation for `:cookie-set`

Default: `nil`, meaning the token cookies are passed according to the `:domain`

## `:cookie-same-site` (optional)
Pass in `:lax` if you want CORS requests pointing to the app to include the token cookies, or `:strict` to avoid that. Only used with the default implementation for `:cookie-set`

Default: `:lax`

## License

Copyright © 2019 Carlo Sciolla

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.

[oidc]: https://openid.net/connect/
[ring]: https://github.com/ring-clojure/ring
[cook]: https://ring-clojure.github.io/ring/ring.middleware.cookies.html#var-wrap-cookies
[sess]: https://ring-clojure.github.io/ring/ring.middleware.session.html#var-wrap-session
[samp]: ./sample-project
