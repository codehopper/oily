# ring-webapp

A sample Ring application to demonstrate how to use the Google OAuth middleware

## Usage

You need to retrieve your OAuth credentials and put it in an EDN file like the following:

```clojure
{:client-id "...."
 :client-secret "...."}
```

Run the sample server as follows:

    $ clj -m sample-project.ring-webapp path/to/credentials-file
