(ns sample-project.ring-webapp
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.util.request :as request]
            [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [codehopper.oidc :as oidc]))

(def server (atom nil))

(defn static-handler
  [req]
  (let [claims (::oidc/id-token req)]
    {:status 200
     :body (str "<p>Here are your claims:</p>"
                "<pre>" (with-out-str (pp/pprint claims)) "</pre>")
     :headers {"Content-Type" "text/html"}}))

(defn start-server
  [handler]
  (reset! server (jetty/run-jetty handler {:port  8000
                                           :join? false})))

(defn stop-server [server]
  (.stop server))

(defn -main [creds-file]
  (let [f (io/file creds-file)]
    (if (.exists f)
      (let [{:keys [client-id secret]} (read-string (slurp f))
            app (-> static-handler
                    (oidc/wrap-oidc {;; the mandatory OAuth credentials
                                     :client-id     client-id
                                     :client-secret secret

                                     ;; make local testing easier by skipping SSL
                                     :cookie-secure false

                                     ;; override some defaults
                                     :redirect-to   "/"
                                     :callback-url  "http://local.codehopper.nl/cb"})
                    (wrap-params)
                    (wrap-cookies)
                    (wrap-session))
            server (start-server app)]
        (try
          (println "Server started, enter any key to quit" client-id)
          (read-line)
          (finally
            (stop-server server))))
      (throw (ex-info "You need to provide a valid credentials file" {:given-file creds-file})))))
