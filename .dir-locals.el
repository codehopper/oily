;;; .dir-locals.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Carlo Sciolla

;; Author: Carlo Sciolla <skuro@margiani>
;; Keywords: abbrev, abbrev, abbrev,
((nil . ((clojure-align-forms-automatically . t) ; enable vertical alignment
         (cljr-favor-prefix-notation . nil) ; do not use prefix notation in :require
         (cljr-clojure-test-declaration . "[clojure.test :refer [deftest testing is]]"))))
