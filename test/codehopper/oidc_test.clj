(ns codehopper.oidc-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]

            [buddy.core.codecs.base64 :as b64]
            [buddy.sign.jws :as jws]
            [buddy.sign.jwt :as jwt]

            [codehopper.oidc :refer :all]))

;;
;; Mocks
;;

(defn oidc-mocks [mock-url]
  ({:oauth-url     {}
    :token-url     {}
    :jwks-url      {:keys [{:alg "RSA"
                            :kid "mock"
                            :typ "mock"
                            :e   (b64/encode "2")
                            :n   (b64/encode "3")}]}
    :discovery-url {:authorization-endpoint :oauth-url
                    :token-endpoint         :token-url
                    :jwks-uri               :jwks-url}} mock-url))

(defmacro with-oidc-mocks
  "Installs mocks for each google interaction"
  [& forms]
  `(with-redefs [oauth-base-url  (constantly :oauth-url)
                 oauth-token-url (constantly :token-url)
                 jwks-url        (constantly :jwks-url)
                 *discovery-url* (constantly :discovery-url)
                 slurp-json             ~oidc-mocks]
     ~@forms))

;;
;; Actual tests
;;

(deftest correct-json-parsing
  (testing "Can parse JSON into the expected clojure format"
    (let [test-file (slurp (io/resource "sample.json"))]
      (is (= {:with-underscore 42
              :nested {:another-underscore "foo"}}
             (parse-json test-file))))))

(deftest resolve-key-test
  (testing "RSA keys can be resolved from the JWKS document"
    (with-oidc-mocks
      (is (some? (resolve-key {:alg "RSA"}))))))

(deftest state-token-generation
  (testing "Random tokens can be generated which are not equals to one another"
    (is (apply not= (take 100 (repeatedly state-token))))))

(deftest parse-id-token-test
  (testing "A valid ID token can be validated and parsed"
    (with-oidc-mocks
      (let [signing-key (resolve-key {:alg "RSA"})
            payload     {"sub"  "12345"
                         "name" "name"
                         "iss"  "mock"}
            id-token    (jwt/sign payload signing-key)]
        (is (= payload (parse-id-token id-token)))))))

(deftest compose-callback-url-test
  (testing "The default callback points to <active-request-url>/cb"
    (let [http-request  {:scheme :http
                         :server-name "localhost"}
          https-request (assoc http-request :scheme :https)
          proxy-request (assoc-in http-request [:headers "x-forwarded-proto"] "https")]
      (is (= "http://localhost/cb" (compose-callback-url http-request)))
      (is (= "https://localhost/cb" (compose-callback-url https-request)))
      (is (= "https://localhost/cb" (compose-callback-url proxy-request))))))

(deftest gen-state-test
  (testing "Default state generation includes the trigger URL"
    (let [trigger-request {:uri "/private/content"
                           :query-string "question=universe&answer=42"}]
      (is (re-matches #"[0-9]+\|/private/content\?question=universe&answer=42" (gen-state trigger-request nil))))))

(deftest saved-trigger-url-test
  (testing "The trigger URL is the relative URL from the OAuth `state`"
    (let [opts {:retrieve-state (constantly "42|/private/content?question=universe&answer=42")}]
      (is (= "/private/content?question=universe&answer=42" (saved-trigger-url nil opts))))))
